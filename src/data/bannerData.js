const bannerData = [
    {
        id: "bannerHome",
        name: "Zuitt Coding Bootcamp",
        description: "Opportunities for everyone, everywhere.",
        button: "Enroll Now!",
        dest: "register"
    },
    {
        id: "bannerError",
        name: "Error 404 - Page not found.",
        description: "The page you are looking for cannot be found.",
        button: "Back to home.",
        dest: ""
    }
]

export default bannerData;