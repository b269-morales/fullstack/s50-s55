import {Link, NavLink} from 'react-router-dom';
import {useState, useContext} from 'react';
import {Nav, Navbar, Container} from 'react-bootstrap';

import UserContext from '../UserContext'


export default function AppNavbar() {

  // to store user information stored in the login page
  // const [user, setUser]= useState(localStorage.getItem('email'));

  const {user} = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg" className="px-3">
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" >
          <Nav>
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/course">Courses</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Collapse id="basic-navbar-nav"  className="justify-content-end" >
          <Nav className="ml-auto">
            { (user.id !== null) ?
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              </>
            }
          </Nav>
           </Navbar.Collapse>
    </Navbar>
  );
}

